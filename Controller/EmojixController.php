<?php

namespace XLabs\EmojixBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use XLabs\MMAdminBundle\Annotations as XLabsMMAdmin;

/**
 * @Route(name="xlabs_emojis_")
 * @XLabsMMAdmin\isProtected
 */
class EmojixController extends Controller
{
    /**
     * @Route("/categorize", name="form_categorize", options={"expose"=true})
     */
    public function categorizeAction(Request $request, KernelInterface $kernel)
    {
        $web_folder = $kernel->getRootDir().'/../web/';
        $json_file = 'bundles/xlabsemojix/emojis,json';
        dump($web_folder, $json_file); die;
    }
}
