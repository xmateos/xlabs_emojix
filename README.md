## Installation ##
Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/emojixbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\EmojixBundle\XLabsEmojixBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml

x_labs_emojix:
    resource: "@XLabsEmojixBundle/Resources/config/routing.yml"
    prefix:   /
```