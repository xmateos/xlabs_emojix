<?php

namespace XLabs\EmojixBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\ProgressBar;
use \SplFileObject;
use \Exception;

class CrawlerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:emojix:crawler')
            ->setDescription('Pull list from official unicode txt file.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        //$web_folder = $container->get('kernel')->getRootDir().'/../web/';
        //$stored_txt = $web_folder.'emojis.txt';

        $stored_txt = dirname(dirname(__FILE__)).'/Resources/public/emojis.txt';
        $json_output = dirname(dirname(__FILE__)).'/Resources/public/emojis.json';
        $json = array();
        $json_emoji = array(
            'code' => false,
            'type' => false,
            'name' => false,
        );

        // Download
        //$remote_file = 'https://unicode.org/Public/emoji/14.0/emoji-sequences.txt';
        //$txt = file_get_contents($remote_file);
        //file_put_contents($stored_txt, $txt);

        $file = new SplFileObject($stored_txt);
        while(!$file->eof())
        {
            $line = $file->fgets();
            $line = str_replace("\n", '', $line);
            $comment = substr($line, 0, 1) == '#' ? true : false;
            if(!$comment && $line != '')
            {
                $exp_line = explode(';', $line);
                $code = trim($exp_line[0]);
                $type = strtolower(trim($exp_line[1]));
                $name = strtolower(trim(substr($exp_line[2], 0, strpos($exp_line[2], '#'))));
                $exp_code = explode('..', $code);
                foreach($exp_code as $code)
                {
                    $emoji_item = array(
                        'code' => $code,
                        'type' => $type,
                        'name' => $name,
                    );
                    //dump($emoji_item); die;
                    $json[] = $emoji_item;
                    /*try {
                        file_put_contents($json_output, json_encode($json));
                    } catch (Exception $e) {
                        dump($e); die;
                    }*/
                }
            }
        }
        $file = null;
        file_put_contents($json_output, json_encode($json));
        $output->writeln("\n-----------------------------------------------------\n");
    }
}